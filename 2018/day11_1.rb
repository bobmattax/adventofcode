

class FuelGrid
  attr_accessor :serial_number,:grid,:cells
  @grid = nil
  @serial_number = nil
  @cells = []
  @blocks = nil
  def initialize(serial_number)
    @serial_number = serial_number
    @cells = []
    @grid = Array.new(300) { Array.new(300,nil) }
    @blocks = Array.new(297) { Array.new(297,nil)}
    fill_grid
    calc_blocks(3,3)
  end

  def add_cell(cell)
    @grid[cell.x][cell.y] = cell
    cell.fuel_grid = self
    cell.power = cell.power_level
    cell
  end

  def max_block
    @grid.flatten.max_by{|c| c.block_power }
  end

  def fill_grid
    @grid.each_with_index do |row,y|
      row.each_with_index do |cell,x|
        c = add_cell(Cell.new(x,y))
      end
    end
  end

  def calc_blocks(w,h)
    puts "Calculating blocks"
    @grid.each_with_index do |row,y|
      row.each_with_index do |col,x|
        if ((x > (@grid[0].size - (w))) or (y > (@grid.size - (h))))
          next
        end
        val = col.calc_block(w,h)
      end
    end
  end

  def calc_block(x,y,w,h)
    coll = []
    (y..y+h-1).each do |y|
      (x..x+w-1).each do |x|
        # puts "at:  #{x},#{y}"
        # puts "pushing #{@grid[x][y].power}"
        coll.push(@grid[x][y].power.to_i)
      end
    end
    coll.inject(0){|sum,x| sum += x.to_i}
  end

  def visualize
    puts "========================================= "
    puts "Grid size: #{@grid.size} x #{@grid[0].size}"
    @grid.each do |row|
      str = ""
      row.each do |col|
        str += col.power.to_s.ljust(4)
      end
      puts str
    end
  end

  def visualize_block(x,y,w,h)
    puts "========================================= "
    (y..y+h-1).each do |j|
      str = ""
      (x..x+w-1).each do |i|
        str += @grid[i][j].power.to_s.rjust(4)
      end
      puts str
    end
  end
end


class Cell
  attr_accessor :x, :y, :fuel_grid, :power, :block_power
  @x = nil
  @y = nil
  @fuel_grid = nil
  @power = nil
  @block_power = nil

  def initialize(x,y)
    @x = x.to_i
    @y = y.to_i
    @power = 0
    @block_power = 0
  end

  def rack_id
    @x + 10
  end

  def power_level
    res = ((rack_id * @y.to_i) + @fuel_grid.serial_number.to_i) * rack_id
    res = res > 100 ? res.to_s.split("")[-3] : 0
    res.to_i - 5
  end

  def calc_block(w,h)
      @block_power = @fuel_grid.calc_block(self.x,self.y,w,h)
    return @block_power
  end

  def inspect
    "#<Cell:#{"0x00%x" % (object_id << 1)} x: #{@x} y: #{@y} power : #{@power} block_power: #{@block_power}>"
  end
end

grid = FuelGrid.new(8141)
max = grid.max_block
puts max.inspect
grid.visualize_block((max.x-1),(max.y-1),5,5)


# puts "Rack ID: #{cell.rack_id}"
# puts "Power level: #{cell.power_level}"
