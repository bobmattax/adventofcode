input = "initial state: .#..##..#.....######.....#....####.##.#.#...#...##.#...###..####.##.##.####..######......#..##.##.##

#.... => .
.##.# => #
#..## => .
....# => .
###.# => #
...#. => #
#...# => #
#.### => .
.#... => #
...## => .
..### => .
####. => .
##.## => .
..##. => .
.#.## => #
#..#. => #
..... => .
#.#.. => .
##.#. => #
.#### => #
##### => .
#.##. => #
.#..# => #
##... => .
..#.# => #
##..# => #
.###. => .
.#.#. => #
#.#.# => #
###.. => .
.##.. => .
..#.. => ."

short_input = "initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"


class Cave
  attr_accessor :state
  @global_shift = 0
  @patterns = nil
  @init_string = ""
  @pots = nil
  @sum = nil
  @state = ""
  def initialize(input)
    @patterns = {}
    parse_input(input)
    puts @patterns.inspect
    @pots = Array.new(@state.size,nil)
    @init_string.chars.each_with_index{|c,idx| @pots[idx] = Pot.new(idx,c)}
  end

  def advance(start)
    # puts "START: #{start}"
    finish = ""
    start.chars.each_with_index do |c,idx|
      #chunk = part to be eval
      chunk = ""
      if (idx-2) < 0
        chunk = start[0..(idx+2)]
        chunk = chunk.rjust(5, '.')
      elsif (idx+2) >= start.length
        chunk = start[(idx-2)..(start.length-1)]
        #pad w/ right "."
        chunk = chunk.ljust(5, '.')
      else
        chunk = start[(idx-2)..(idx+2)]
      end
      # puts "#{idx} ==> #{chunk}"
      finish[idx] = pattern_lookup(chunk)
    end
    # puts "AFTER ADVANCE: #{finish}"
    @state = finish
  end

  def pattern_lookup(chunk)
    if(@patterns[chunk])
      return @patterns[chunk]
    else
      return "."
    end
  end

  def parse_input(input)
    lines = input.split("\n")
    init = lines.shift
    matches = /initial state: ([\#\.]+)/.match(init)
    # puts "init state: #{matches[1]}"
    @init_string = matches[1]
    garbage = lines.shift
    lines.each do |line|
      matches = /([\#\.]{5}) => ([\#\.])/.match(line)
      # puts matches[2]
      @patterns[matches[1]] = matches[2]
    end
    @state = @init_string
    @init_string.chars.each_with_index{|c,idx| @pots[idx] = Pot.new(idx,c)}
  end


end

class Pot
  @plant = false
  @id = nil
  def initialize(idx,plant_char)
    @id = idx
    @plant = (plant_char == "#" ) ? true : false
  end
end


test = 1
cave = Cave.new(test == 1 ? short_input : input)
gen = 0
puts "CAVE[#{gen}]#{cave.state}"
loop do
  gen += 1
  cave.advance(cave.state)
  puts "CAVE[#{gen}]#{cave.state}"
  break if gen > 4
end
