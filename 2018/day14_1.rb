class Elf
  attr_accessor :current_recipe, :current_recipe_pos, :id
  @current_recipe = nil
  @current_recipe_pos = nil
  @id = nil
  def initialize(id)
    @id = id
  end
end

class Recipe
  attr_accessor :quality
  @id = nil
  @quality = nil
  def initialize(q)
    @quality = q.to_i
  end
end

class Sim
  attr_accessor :run_count

  @elves = []
  @recipes = []
  @run_count = 0
  def initialize(elves,starting_recipes)
    @elves = []
    @recipes = []
    @run_count = 0
    elves.to_i.times.with_index{ |x| @elves << Elf.new(x) }
    starting_recipes.map{|x| @recipes.push Recipe.new(x)}
    @elves[0].current_recipe = @recipes[0]
    @elves[0].current_recipe_pos = 0
    @elves[1].current_recipe = @recipes[1]
    @elves[1].current_recipe_pos = 1
  end

  def visualize(starting_pos,number)
    pos = starting_pos
    str = ""
    (starting_pos..(starting_pos+number-1)).each_with_index do |i,idx|
      # puts "#{idx}:#{@recipes[i].inspect}"
      str += @recipes[i].quality.to_s
    end
    puts str
  end

  def to_s
    @recipes.collect{|x| x.quality}.join
  end

  def run(num)
    # puts "running #{@run_count}"
    num.to_i.times.each do |x|
      sum = @elves.inject(0){|sum,e| sum += e.current_recipe.quality }
      # puts "RUN: #{x} | SUM: #{sum}"
      sum.to_s.chars.each do |c|
        @recipes.push(Recipe.new(c))
      end
      @elves.each do |elf|
        new_recipe(elf)
      end
      @run_count += 1
    end
  end

  def run_until(num)
    loop do
      puts "run count: #{@run_count}"
      run(1)
      break if @recipes.size >= num
    end
  end

  def run_looking_for(str)
    last_tick = Time.now
    loop do
      # puts "Run: #{run_count} Looking for #{str} -- #{to_s}"
      if @run_count % 10000 == 0
        puts "#{@run_count}... #{Time.now - last_tick}"
        last_tick = Time.now
      end
      if /#{str}/.match(to_s)
        splits = to_s.split(str)
        left_len = splits[0].size

        puts "Found #{str} at #{@run_count} len: #{left_len}"
        break
      end
      run(10000)
      # break if @run_count > 15
    end
  end

  def new_recipe(elf)
    moves = 1 + elf.current_recipe.quality
    new_pos = elf.current_recipe_pos + moves
    mod_new_pos = new_pos % @recipes.size
    elf.current_recipe = @recipes[mod_new_pos]
    elf.current_recipe_pos = mod_new_pos
  end
end


sim = Sim.new(2,[3,7])
# sim.run_until(540491)
sim.run_looking_for("540391")
# sim.visualize(540391,10)
