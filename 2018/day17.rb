sample = "x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504"

data = File.readlines('day17.txt').join

class Coord
  attr_accessor :x, :y, :type
  @x = nil
  @y = nil
  @type = nil
  def initialize(x,y,type=nil)
    @x = x.to_i
    @y = y.to_i
    if !type.nil?
      @type = type
    end
  end
end

class Simulation
  @clay = nil
  @max_x = nil
  @max_y = nil
  @min_y = nil
  @min_x = nil
  @grid = nil
  def initialize(input)
    @coords = []
    parse_input(input)
    put_spring(500,0)
    max_dims
    min_dims
    @grid = new_grid
    make_grid
  end

  def new_grid
    Array.new(@max_y+1) { Array.new(@max_x+1,nil) }
  end

  def parse_input(input)
    lines = input.split("\n")
    lines.each do |line|
      coords =
      coord_parts = line.split(", ")
      static_parts = coord_parts[0].split("=")
      static_key = static_parts[0]
      range_parts = coord_parts[1].split("=")
      range_key = range_parts[0]
      eval(range_parts[1]).each do |i|
        coord = {}
        coord[range_key] = i
        coord[static_key] = static_parts[1]
        # puts coord.inspect
        @coords.push(Coord.new(coord["x"],coord["y"], :clay))
      end
    end
  end

  def put_spring(x,y)
    @coords.push(Coord.new(500,0,:spring))
  end

  def visualize
    (@min_y..@max_y).each do |y|
      str = ""
      (@min_x..@max_x).each do |x|
        if @grid[y][x].nil?
          str += "."
          next
        end
        str += @grid[y][x]
      end
      puts str
    end
  end

  def max_dims
    @max_x = @coords.max_by{|c| c.x }.x
    @max_y = @coords.max_by{|c| c.y }.y
  end

  def min_dims
    @min_x = @coords.min_by{|c| c.x }.x
    @min_y = @coords.min_by{|c| c.y }.y
  end

  def type_to_str(type)
    case type
      when :clay
        "#"
      when :spring
        "+"
      else
        "."
    end
  end

  def make_grid
    # puts @grid.inspect

    @coords.each do |c|
      # puts "Placing #{c.x}, #{c.y}"
      @grid[c.y][c.x] = type_to_str(c.type)
    end
    @grid.each_with_index do |row,y|
      row.each_with_index do |col,x|
        if @grid[y][y].nil?
          @grid[y][x] = "."
        end
      end
    end
  end

  def tick
    # puts "ticking..."
    after = new_grid
    after.each_with_index do |row,y|
      row.each_with_index do |col,x|
        new_pos = set_pos(x,y)
        after[y][x] = new_pos
      end
    end
    @grid = after
  end
end

sim = Simulation.new(data)
# puts sim.inspect
sim.visualize
