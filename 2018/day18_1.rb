data = File.readlines('day18.txt').join
sample = ".#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|."


class Spot
end

class Forest

end

class Simulation
  @time = 0
  @grid = nil
  @width = 0
  @height = 0
  def initialize(input)
    @time = 0
    parse_input(input)
  end

  def score

    lumberyards = @grid.flatten.select{|x| x == "#"}.size
    trees = @grid.flatten.select{|x| x == "|"}.size
    puts "Lumber: #{lumberyards} Trees: #{trees} Score: #{lumberyards*trees}"
  end

  def get_dims(input)
    lines = input.split("\n")
    @width = lines[0].size
    @height = lines.size
    @grid = Array.new(@height) { Array.new(@width,nil) }
  end

  def parse_input(input)
    get_dims(input)
    input.split("\n").each_with_index do |line,y|
      line.chars.each_with_index do |col,x|
        @grid[y][x] = col
      end
    end
  end

  def visualize(grid=nil)
    puts "==================================="
    if grid.nil?
      grid = @grid
    end
    grid.each do |row|
      line = ""
      row.each do |col|
        line += col
      end
      puts line
    end
    puts "==================================="
  end

  def tick
    # puts "ticking..."
    after = Array.new(@height) { Array.new(@width,".") }
    after.each_with_index do |row,y|
      row.each_with_index do |col,x|
        new_pos = set_pos(x,y)
        after[y][x] = new_pos
      end
    end
    @grid = after
  end

  def set_pos(x,y)
    current = @grid[y][x]
    # puts "Current: #{x},#{y} = #{current}"
    counts = counts(x,y)
    if current == "."
      if counts[:trees] >= 3
        return "|"
      end
    end
    if current == "|"
      if counts[:lumberyards] >= 3
        return "#"
      end
    end
    if current == "#"
      if counts[:lumberyards] < 1 || counts[:trees] < 1
        return "."
      end
    end
    return current
  end

  def counts(x,y)
    trees = 0
    lumberyards = 0
    open = 0
    (y-1..y+1).each do |i|
      (x-1..x+1).each do |j|
        next if j < 0 || j > (@width - 1 )|| i < 0 || i > (@height - 1)
        next if x == j && y == i
        spot = @grid[i][j]
        # puts "Current: (#{j},#{i})#{spot}"
        if spot == "."
          open += 1
        elsif spot == "#"
          lumberyards += 1
        elsif spot == "|"
          trees += 1
        else
          #something else?!
        end
      end
    end
    return {trees: trees, lumberyards: lumberyards, open: open}
  end

  def run(num_ticks)
    count = 0
    last_tick = Time.now
    loop do
      if @time % 1000 == 0
        puts "#{@time}... #{Time.now - last_tick}"
        last_tick = Time.now
        score
      end
      # do stuff
      tick
      count += 1
      @time += 1
      # puts "Plot after #{@time}"
      # visualize
      break if count >= num_ticks
    end
  end

  def count_pos(x,y)
    puts "Counts for (#{x}, #{y})"
    puts counts(x,y)
  end

end



sim = Simulation.new(data)

# puts sim.counts(7,0)
# puts sim.counts(8,0)
sim.run(1000000000)
sim.visualize()
sim.score
# sim.visualize()
# sim.run(1)
