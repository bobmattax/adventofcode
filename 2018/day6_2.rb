input = "267, 196
76, 184
231, 301
241, 76
84, 210
186, 243
251, 316
265, 129
142, 124
107, 134
265, 191
216, 226
67, 188
256, 211
317, 166
110, 41
347, 332
129, 91
217, 327
104, 57
332, 171
257, 287
230, 105
131, 209
110, 282
263, 146
113, 217
193, 149
280, 71
357, 160
356, 43
321, 123
272, 70
171, 49
288, 196
156, 139
268, 163
188, 141
156, 182
199, 242
330, 47
89, 292
351, 329
292, 353
290, 158
167, 116
268, 235
124, 139
116, 119
142, 259"

input_short = "1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"

class Coord
  attr_accessor :x,:y,:id,:primary,:closest_distance,:closest_id,
    :infinite,:edge,:area,:total_dist
  @x = nil
  @y = nil
  @id = nil
  @primary = nil
  @area = nil
  @closest_distance = nil
  @closest_id = nil
  @infinite = nil
  @edge = nil
  @total_dist = nil
  def initialize(id,x,y)
    @id = id
    @x = x
    @y = y
  end
end

class Plane
  attr_accessor :coords,:grid
  @coords = nil
  @max_x = nil
  @max_y = nil
  @grid = nil

  def initialize(coords)
    @coords = coords
    self.max_dims
    puts "Making grid"
    @grid = Array.new(@max_y + 1) { Array.new(@max_x + 1,nil) }
    puts "Assigning input to grid"
    self.coords_on_grid
    puts "Traversing..."
    self.traverse
    # puts "Calculating area"
    # self.calc_area
    puts "Determining edges"
    self.edge_coords
  end

  def primary_coords
    @coords.select{|x| x.primary == true }
  end

  def edge_coords
    edges = []
    # each north
    @grid[0].each{|x| edges << x.closest_id}
    # each west
    @grid.each{|y|
      edges << y[0].closest_id
      edges << y[@max_x].closest_id
    }
    # each east
    # each south
    @grid[@max_y].each{|x| edges << x.closest_id}
    edges.uniq
  end

  def max_dims
    @max_x = @coords.max_by{|x| x.x}.x.to_i
    @max_y = @coords.max_by{|x| x.y}.y.to_i
    puts "MAX DIMS (#{@max_x},#{@max_y})"
  end

  def coords_on_grid
    @coords.each do |coord|
      @grid[coord.y][coord.x] = coord
    end
  end

  def coord_distance(start,finish)
    # puts "calc distance between:"
    # puts start.inspect
    # puts finish.inspect
    ( start.x - finish.x ).abs + (start.y - finish.y).abs
  end

  def closest_coord(coord)
    # puts "Looking for closest coord for #{coord.inspect}"
    distances = {}
    primary_coords.each do |prim|
      dist = coord_distance(prim,coord)
      distances[dist] = distances[dist].nil? ? [prim.id] : distances[dist].push(prim.id)
      distances
    end
    # puts "CLOSEST: #{distances.min_by{|k,v| k}} "
    closest = distances.min_by{|k,v| k}
    # puts "Tie? #{closest[1].size}"
    if closest[1].size == 1
      coord.closest_distance = closest[0]
      coord.closest_id = closest[1][0]
      # puts "closest is...#{closest[0]} away [#{coord.closest_id}]"
    else
      coord.closest_distance = nil
      coord.closest_id = nil
    end
  end

  def distance_to_all(coord)
    dist = primary_coords.inject(0){|sum,x| sum + coord_distance(x,coord) }
    puts "TOTAL DIST [#{coord.y},#{coord. x}]: #{dist}"
    coord.total_dist = dist
  end

  def calc_area
    primary_coords.each do |prim|
      puts "Calculating area for #{prim.id}..."
      puts "Total coords: #{@coords.size}"
      area = @grid.flatten.select{|x| x.primary == false && x.closest_id == prim.id }
      prim.area = area.size + 1
    end
  end

  def calc_region
    plane.grid.flatten.select{|x| x.total_dist.nil?}.inspect
  end

  def traverse
    i = 0
    max = @max_x * @max_y
    @grid.each_with_index do |y,y_idx|
      y.each_with_index do |x,x_idx|
        i += 1
        if i%1000 == 0
          puts "#{i} / #{max}"
        end
        if x.is_a?(Coord)
          self.distance_to_all(x)
          next
        end
        c = Coord.new("",x_idx,y_idx)
        c.primary = false
        # self.closest_coord(c)
        self.distance_to_all(c)
        @grid[y_idx][x_idx] = c
      end
    end
  end

  def visualize
    @grid.each do |y|
      line = ""
      y.each do |x|
        if x.primary == true
          line += "*"
        elsif x.closest_id.nil?
          line += "."
        else
          line += " "
        end
      end
      puts line
    end
  end
end

input_coords = input.split("\n").each_with_index.collect do |str,idx|
  x,y = str.split(",")
  c = Coord.new(idx,x.to_i,y.to_i)
  c.primary = true
  c
end

plane = Plane.new(input_coords)
# puts plane.inspect
plane.visualize
# plane.edge_coords.each do |x|
#   c = plane.primary_coords.select{|c| c.id == x}
#   c.each{|x| x.infinite = true}
# end
puts plane.grid.flatten.select{|x| !x.total_dist.nil? && x.total_dist < 10000}.size

# puts plane.coords.select{|x| x.total_dist < 32}.inspect
# results = {}
# plane.primary_coords.select{|x| x.infinite.nil? }.each do |c|
#   results[c.id] = {id: c.id, area: c.area.to_i}
# end
# puts "MAX" + "#{results.max_by{|k,v| v[:area]}}"
