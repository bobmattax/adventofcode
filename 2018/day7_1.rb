input = "Step G must be finished before step N can begin.
Step N must be finished before step B can begin.
Step P must be finished before step Q can begin.
Step F must be finished before step U can begin.
Step H must be finished before step A can begin.
Step C must be finished before step S can begin.
Step A must be finished before step K can begin.
Step M must be finished before step O can begin.
Step V must be finished before step L can begin.
Step E must be finished before step L can begin.
Step B must be finished before step Q can begin.
Step W must be finished before step J can begin.
Step R must be finished before step D can begin.
Step D must be finished before step S can begin.
Step S must be finished before step X can begin.
Step Q must be finished before step J can begin.
Step I must be finished before step L can begin.
Step U must be finished before step J can begin.
Step Z must be finished before step X can begin.
Step Y must be finished before step T can begin.
Step J must be finished before step K can begin.
Step T must be finished before step L can begin.
Step K must be finished before step O can begin.
Step O must be finished before step X can begin.
Step L must be finished before step X can begin.
Step Y must be finished before step O can begin.
Step F must be finished before step S can begin.
Step K must be finished before step L can begin.
Step Z must be finished before step O can begin.
Step J must be finished before step X can begin.
Step K must be finished before step X can begin.
Step Q must be finished before step X can begin.
Step Y must be finished before step L can begin.
Step E must be finished before step S can begin.
Step H must be finished before step Y can begin.
Step G must be finished before step P can begin.
Step E must be finished before step K can begin.
Step B must be finished before step L can begin.
Step T must be finished before step K can begin.
Step N must be finished before step R can begin.
Step F must be finished before step E can begin.
Step W must be finished before step Y can begin.
Step U must be finished before step X can begin.
Step A must be finished before step I can begin.
Step Q must be finished before step Y can begin.
Step P must be finished before step T can begin.
Step D must be finished before step X can begin.
Step E must be finished before step Y can begin.
Step F must be finished before step B can begin.
Step P must be finished before step I can begin.
Step N must be finished before step S can begin.
Step F must be finished before step V can begin.
Step W must be finished before step U can begin.
Step F must be finished before step A can begin.
Step I must be finished before step Z can begin.
Step E must be finished before step D can begin.
Step R must be finished before step I can begin.
Step M must be finished before step V can begin.
Step R must be finished before step U can begin.
Step R must be finished before step X can begin.
Step G must be finished before step O can begin.
Step G must be finished before step H can begin.
Step M must be finished before step R can begin.
Step E must be finished before step U can begin.
Step F must be finished before step Z can begin.
Step N must be finished before step Q can begin.
Step U must be finished before step O can begin.
Step J must be finished before step T can begin.
Step W must be finished before step Z can begin.
Step I must be finished before step J can begin.
Step U must be finished before step L can begin.
Step I must be finished before step X can begin.
Step Z must be finished before step J can begin.
Step F must be finished before step D can begin.
Step N must be finished before step O can begin.
Step Q must be finished before step U can begin.
Step G must be finished before step L can begin.
Step H must be finished before step Q can begin.
Step M must be finished before step Q can begin.
Step N must be finished before step D can begin.
Step Z must be finished before step L can begin.
Step I must be finished before step Y can begin.
Step E must be finished before step X can begin.
Step J must be finished before step L can begin.
Step H must be finished before step W can begin.
Step P must be finished before step Y can begin.
Step Q must be finished before step T can begin.
Step Z must be finished before step Y can begin.
Step R must be finished before step T can begin.
Step E must be finished before step J can begin.
Step I must be finished before step T can begin.
Step A must be finished before step L can begin.
Step E must be finished before step R can begin.
Step T must be finished before step O can begin.
Step Y must be finished before step X can begin.
Step A must be finished before step Q can begin.
Step W must be finished before step Q can begin.
Step A must be finished before step T can begin.
Step B must be finished before step Y can begin.
Step H must be finished before step E can begin.
Step H must be finished before step K can begin."

short_input = "Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."

class Tree
  attr_accessor :nodes
  @nodes = nil
  def initialize()
    @nodes = []
  end

  def find_node(id)
    puts "Looking for node w/in Tree [#{id}]"
    nodes.select{|n| n.id == id}[0]
  end

  def traverse
    path = []
    #find empty preqs
    count = 0
    loop do
      count += 1
      puts "Loop start =================== "
      puts "current path #{path.join}"
      ready = no_prereqs_left[0]
      if !ready.is_a?(Node)
        break
      end
      puts "Ready with: #{ready}"
      path.push(ready.id)
      ready.done = true

      waiting_on(ready).each do |n|
        n.prereqs = n.prereqs - [ready]
      end
    end
  end

  def no_prereqs_left
    preqs = nodes.select{|x| x.prereqs.empty? && x.done == false}
    puts "NO PREREQS: #{preqs}"
    return preqs.empty? ? [] : preqs.sort_by{|x| x.id}
  end

  def waiting_on(n)
    waiting = nodes.select{|x| x.prereqs.include?(n)}
    puts "WAITING: #{waiting}"
    return waiting.sort_by{|x| x.id}
  end
end

class Node
  attr_accessor :prereqs, :id, :done
  @prereqs = nil
  @id = nil
  @done = false
  def initialize(id)
    @prereqs = []
    @id = id
    @done = false
  end
end

instruction_lines = input.split("\n")
prereqs = instruction_lines.collect do |line|
  matches = /Step (.) must be finished before step (.) can begin./.match(line)
  {matches[1] => matches[2]}
end

#create the nodes
node_ids = []
prereqs.map do |p|
  k = p.keys[0]
  node_ids << k
  node_ids << p[k]
end

tree = Tree.new

node_ids.sort.uniq.each do |n|
  tree.nodes.push(Node.new(n))
end

prereqs.each do |item|
  puts item
  k = item.keys[0]
  v = item[k]
  prereq = tree.find_node(k)
  node = tree.find_node(v)

  node.prereqs << prereq
end

tree.traverse

tree.nodes.each do |n|
  puts n.inspect
end
