input = "Step G must be finished before step N can begin.
Step N must be finished before step B can begin.
Step P must be finished before step Q can begin.
Step F must be finished before step U can begin.
Step H must be finished before step A can begin.
Step C must be finished before step S can begin.
Step A must be finished before step K can begin.
Step M must be finished before step O can begin.
Step V must be finished before step L can begin.
Step E must be finished before step L can begin.
Step B must be finished before step Q can begin.
Step W must be finished before step J can begin.
Step R must be finished before step D can begin.
Step D must be finished before step S can begin.
Step S must be finished before step X can begin.
Step Q must be finished before step J can begin.
Step I must be finished before step L can begin.
Step U must be finished before step J can begin.
Step Z must be finished before step X can begin.
Step Y must be finished before step T can begin.
Step J must be finished before step K can begin.
Step T must be finished before step L can begin.
Step K must be finished before step O can begin.
Step O must be finished before step X can begin.
Step L must be finished before step X can begin.
Step Y must be finished before step O can begin.
Step F must be finished before step S can begin.
Step K must be finished before step L can begin.
Step Z must be finished before step O can begin.
Step J must be finished before step X can begin.
Step K must be finished before step X can begin.
Step Q must be finished before step X can begin.
Step Y must be finished before step L can begin.
Step E must be finished before step S can begin.
Step H must be finished before step Y can begin.
Step G must be finished before step P can begin.
Step E must be finished before step K can begin.
Step B must be finished before step L can begin.
Step T must be finished before step K can begin.
Step N must be finished before step R can begin.
Step F must be finished before step E can begin.
Step W must be finished before step Y can begin.
Step U must be finished before step X can begin.
Step A must be finished before step I can begin.
Step Q must be finished before step Y can begin.
Step P must be finished before step T can begin.
Step D must be finished before step X can begin.
Step E must be finished before step Y can begin.
Step F must be finished before step B can begin.
Step P must be finished before step I can begin.
Step N must be finished before step S can begin.
Step F must be finished before step V can begin.
Step W must be finished before step U can begin.
Step F must be finished before step A can begin.
Step I must be finished before step Z can begin.
Step E must be finished before step D can begin.
Step R must be finished before step I can begin.
Step M must be finished before step V can begin.
Step R must be finished before step U can begin.
Step R must be finished before step X can begin.
Step G must be finished before step O can begin.
Step G must be finished before step H can begin.
Step M must be finished before step R can begin.
Step E must be finished before step U can begin.
Step F must be finished before step Z can begin.
Step N must be finished before step Q can begin.
Step U must be finished before step O can begin.
Step J must be finished before step T can begin.
Step W must be finished before step Z can begin.
Step I must be finished before step J can begin.
Step U must be finished before step L can begin.
Step I must be finished before step X can begin.
Step Z must be finished before step J can begin.
Step F must be finished before step D can begin.
Step N must be finished before step O can begin.
Step Q must be finished before step U can begin.
Step G must be finished before step L can begin.
Step H must be finished before step Q can begin.
Step M must be finished before step Q can begin.
Step N must be finished before step D can begin.
Step Z must be finished before step L can begin.
Step I must be finished before step Y can begin.
Step E must be finished before step X can begin.
Step J must be finished before step L can begin.
Step H must be finished before step W can begin.
Step P must be finished before step Y can begin.
Step Q must be finished before step T can begin.
Step Z must be finished before step Y can begin.
Step R must be finished before step T can begin.
Step E must be finished before step J can begin.
Step I must be finished before step T can begin.
Step A must be finished before step L can begin.
Step E must be finished before step R can begin.
Step T must be finished before step O can begin.
Step Y must be finished before step X can begin.
Step A must be finished before step Q can begin.
Step W must be finished before step Q can begin.
Step A must be finished before step T can begin.
Step B must be finished before step Y can begin.
Step H must be finished before step E can begin.
Step H must be finished before step K can begin."

short_input = "Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."

class Worker
  attr_accessor :work, :id
  @work = nil
  @ready = true
  @ready_at = nil
  @id = nil
  # @name = nil
  def initialize(id)
    @ready = true
    @id = id
    # @name = name
    @work = nil
  end
end

class Tree
  attr_accessor :nodes
  @nodes = nil
  @workers = []
  @worktax = nil
  @raw = nil
  @parse = nil
  @prereqs = nil

  def initialize(input, workers,worktax)
    # @names = ["Albert","Bob","Charlie","Dave","Edward"]
    @nodes = []
    @workers = []
    @worktax = worktax
    @raw = input
    (0..(workers-1)).each do |w|
      @workers.push Worker.new(w)
    end
    unique_nodes
  end

  def unique_nodes
    instruction_lines = @raw.split("\n")
    @prereqs = instruction_lines.collect do |line|
      matches = /Step (.) must be finished before step (.) can begin./.match(line)
      {matches[1] => matches[2]}
    end
    #create the nodes
    node_ids = []
    @prereqs.map do |p|
      k = p.keys[0]
      node_ids << k
      node_ids << p[k]
    end

    node_ids.sort.uniq.each do |n|
      self.nodes.push(Node.new(n))
    end

    @prereqs.each do |item|
      puts item
      k = item.keys[0]
      v = item[k]
      prereq = self.find_node(k)
      node = self.find_node(v)

      node.prereqs << prereq
    end
  end

  def find_node(id)
    puts "Looking for node w/in Tree [#{id}]"
    nodes.select{|n| n.id == id}[0]
  end

  def remove_from_prereqs(node)
    waiting_on(node).each do |n|
      n.prereqs = n.prereqs - [node]
    end
  end

  def work
    path = []
    #find empty preqs
    seconds = 0
    loop do
      puts "Loop start =====================================
      Second: #{seconds}"
      curtime = seconds
      # break if seconds > 20
      # finish currently assigned work
      @nodes.select{|x| x.status == :working }.each do |n|
        puts "Assigned node: #{n.inspect}"
        if curtime == n.done_at
          puts "finishing work on #{n.id}"
          path.push(n.id)
          n.status == :done
          n.done = true
          worker = @workers.select{|w| w.work == n}[0].work = nil
          puts "WORKER #{worker.inspect}"
          self.remove_from_prereqs(n)
        end
      end
      seconds += 1
      puts "current path #{path.join}"
      ready = no_prereqs_left
      puts "nodes w/ all prereqs met and status: unassigned"
      puts ready.inspect
      if ready.empty? && path.length == @nodes.size
        puts "No more work: Done after #{curtime} seconds"
        break
      end
      puts "Assigning work to #{@workers.size}"
      @workers.each_with_index do |w|

        if !w.work.nil?
          puts "Worker already busy on #{w.work.id}"
          next
        end
        puts w.inspect
        work = ready.shift
        if work.nil?
          puts "no more work ready this tick[#{curtime}]"
          next
        end

        # do stuff w/ the work
        puts "Ready with: #{work}"
        ttw = (work.id.ord - 64) + @worktax
        puts "Time for work: #{ttw}"
        puts "Will be done on tick #{curtime + ttw }"
        work.done_at = curtime + ttw
        work.assign(w)

        # w.work = work
        # work.done = true
      end
      #
      # waiting_on(ready).each do |n|
      #   n.prereqs = n.prereqs - [ready]
      # end
      # break
    end
  end

  def no_prereqs_left
    preqs = nodes.select{|x| x.prereqs.empty? && x.status == :unassigned}
    puts "NO PREREQS: #{preqs}"
    return preqs.empty? ? [] : preqs.sort_by{|x| x.id}
  end

  def waiting_on(n)
    waiting = nodes.select{|x| x.prereqs.include?(n)}
    puts "WAITING: #{waiting}"
    return waiting.sort_by{|x| x.id}
  end
end

class Node
  attr_accessor :prereqs, :id, :done, :status, :done_at
  @prereqs = nil
  @id = nil
  @done = false
  @status = nil
  @assigned_to = nil
  @done_at = nil
  def initialize(id)
    @prereqs = []
    @id = id
    @done = false
    @status = :unassigned
  end

  def assign(worker)
    @assigned_to = worker
    @status = :working
    worker.work = self
  end
end

# instruction_lines = input.split("\n")
# prereqs = instruction_lines.collect do |line|
#   matches = /Step (.) must be finished before step (.) can begin./.match(line)
#   {matches[1] => matches[2]}
# end
#
# #create the nodes
# node_ids = []
# prereqs.map do |p|
#   k = p.keys[0]
#   node_ids << k
#   node_ids << p[k]
# end

tree = Tree.new(input, 5,60)

# node_ids.sort.uniq.each do |n|
#   tree.nodes.push(Node.new(n))
# end

# prereqs.each do |item|
#   puts item
#   k = item.keys[0]
#   v = item[k]
#   prereq = tree.find_node(k)
#   node = tree.find_node(v)
#
#   node.prereqs << prereq
# end

tree.work

# tree.nodes.each do |n|
#   puts n.inspect
# end
