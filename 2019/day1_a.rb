line_num = 0

def fuel_req (mass)    
    return  (mass / 3) - 2
end
sum = 0
File.open('day1_a.input').each do |line|
    mass = line.chomp.to_i
    fuel = fuel_req(mass)    
    print "#{line_num += 1} : #{mass} => #{fuel} \n"
    sum += fuel    
end

puts "SUM: #{sum}"