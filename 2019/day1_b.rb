line_num = 0

def fuel_req (mass)    
    return  (mass / 3) - 2
end

def fuel_loop (fuel)    
    extra = 0
    x_fuel = fuel_req(fuel)    
    if x_fuel > 0
        extra += x_fuel
        extra += fuel_loop(x_fuel)
    else 
        return 0
    end
end
sum = 0
File.open('day1_b.input').each do |line|
    mass = line.chomp.to_i
    fuel = fuel_req(mass)    
    extra = fuel_loop(fuel)
    print "#{line_num += 1} : #{mass} => #{fuel} \n"
    sum += fuel + extra    
end

puts "SUM: #{sum}"
