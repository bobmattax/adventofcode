
a = IO.readlines("day2.input")
arr = a[0].split(",")
arr = arr.map(&:to_i)

arr[1] = 12
arr[2] = 2

puts arr.inspect

count = 0
loop do
    op_pos = count * 4
    op = arr[op_pos]
    count += 1

    case op
    when 1
        in_1 = arr[arr[op_pos+1]]
        in_2 = arr[arr[op_pos+2]]
        output = arr[op_pos+3].to_i
        puts "add #{in_1} + #{in_2} (#{in_1 + in_2}) => position:#{output}"
        arr[output] = in_1 + in_2

    when 2
        in_1 = arr[arr[op_pos+1]]
        in_2 = arr[arr[op_pos+2]]
        output = arr[op_pos+3]
        arr[output] = in_1 * in_2

        puts "multiply #{arr[in_1]} * #{arr[in_2]} (#{in_1 * in_2}) => position:#{output}"

    when 99
        puts "halt"
        puts "break 1"
        break
    end
    
    if op_pos > arr.size || op == 99
        puts "break 2"
        break
    end
    puts "#{count} : #{op}"
end
puts arr.inspect

puts "FINAL: #{arr[0]}"
