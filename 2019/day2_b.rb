
a = IO.readlines("day2.input")
arr = a[0].split(",")
arr = arr.map(&:to_i)
orig = arr
orig.freeze




# puts arr.inspect


def run(arr,noun,verb)    
    count = 0
    arr[1] = noun
    arr[2] = verb
    loop do
        op_pos = count * 4
        op = arr[op_pos]
        count += 1

        case op
        when 1
            in_1 = arr[arr[op_pos+1]]
            in_2 = arr[arr[op_pos+2]]
            output = arr[op_pos+3].to_i
            # puts "add #{in_1} + #{in_2} (#{in_1 + in_2}) => position:#{output}"
            arr[output] = in_1 + in_2

        when 2
            in_1 = arr[arr[op_pos+1]]
            in_2 = arr[arr[op_pos+2]]
            output = arr[op_pos+3]
            arr[output] = in_1 * in_2

            # puts "multiply #{arr[in_1]} * #{arr[in_2]} (#{in_1 * in_2}) => position:#{output}"

        when 99
            # puts "halt"
            # puts "break 1"
            break
        end
        
        if op_pos > arr.size || op == 99
            # puts "break 2"
            break
        end
        # puts "#{count} : #{op}"
    end
    
    return {value: arr[0], noun: noun, verb: verb}
    # puts "value: #{arr[0]} Noun: #{noun}, Verb: #{verb} "
end

values = {}


for i in 0..99 do 
    for j in 0..99 do
        arr = orig.dup
        # puts "#{i} * #{j}"        
        results = run(arr,i,j)
        # puts results.inspect
        val =  results[:value]
        if values[val]
            values[val] = values[val] + 1
        else
            values[val] = 1
        end
        
        if results[:value] == 19690720
            puts "FOUND IT #{i} * #{j}"
        end
    end
end

puts results[19690720]
# puts values.inspect
# puts orig.inspect
