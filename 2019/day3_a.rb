
graph = {}

def distance (coord)
    return coord[0].to_i.abs + coord[1].to_i.abs
end

class Line
    @cur_x = 0
    @cur_y = 0
    @coords = [{x: 0, y:0}]
    def initialize(line_string)
        @cur_x = 0
        @cur_y = 0
        @coords = [0,0]
        puts line_string
        instructions = line_string.split(",")
        instructions.each do |inst|            
            dir = inst.chars[0]           
            distance = inst.slice(1,inst.chars.size - 1).to_i
            puts "Dir: #{dir} Distance: #{distance}"
            distance.times do |i|
                case dir
                when "U"
                    @cur_y += 1
                    @coords << [@cur_x, @cur_y]
                when "D"
                    @cur_y -= 1
                    @coords << [@cur_x, @cur_y]
                when "L"
                    @cur_x -= 1
                    @coords << [@cur_x, @cur_y]
                when "R"
                    @cur_x += 1
                    @coords << [@cur_x, @cur_y]
                end
            end
        end
        puts "Coords: #{@coords}"
        return self
    end

    def coords
        return @coords
    end
    
end

lines = IO.readlines("day3.input")
paths = []
lines.each do |line|
    paths << Line.new(line)    
end

# puts paths

intersections = (paths[0].coords & paths[1].coords)

low = nil
intersections.each do |inter|
    next if (inter[0]==0 && inter[1] == 0)
    dist = distance(inter)
    if low.nil? || dist < low
        low = dist
    end
end

puts "Best distance: #{low}"