
graph = {}

def distance (coord)
    return coord[0].to_i.abs + coord[1].to_i.abs
end

class Line
    @cur_x = 0
    @cur_y = 0
    @coords = [{x: 0, y:0}]
    def initialize(line_string)
        @cur_x = 0
        @cur_y = 0
        @coords = []
        @coords << [0,0]
        puts line_string
        instructions = line_string.split(",")
        instructions.each do |inst|            
            dir = inst.chars[0]           
            distance = inst.slice(1,inst.chars.size - 1).to_i
            puts "Dir: #{dir} Distance: #{distance}"
            distance.times do |i|
                case dir
                when "U"
                    @cur_y += 1
                    @coords << [@cur_x, @cur_y]
                when "D"
                    @cur_y -= 1
                    @coords << [@cur_x, @cur_y]
                when "L"
                    @cur_x -= 1
                    @coords << [@cur_x, @cur_y]
                when "R"
                    @cur_x += 1
                    @coords << [@cur_x, @cur_y]
                end
            end
        end
        puts "Coords: #{@coords}"
        return self
    end

    def traverse_to(coord)
        target = coord
        steps = 0
        self.coords.each do |c|
            next if (c[0]==0 && c[1] == 0)
            if target[0] == c[0] && target[1] == c[1]
                puts "we found it : #{steps+1} "
                return steps+1
            else
                steps += 1
            end
        end
    end

    def coords
        return @coords
    end
    
end

lines = IO.readlines("day3.sample")
paths = []
lines.each do |line|
    paths << Line.new(line)    
end

# puts paths

intersections = (paths[0].coords & paths[1].coords)

low = nil
best_index = nil
intersections.each_with_index do |inter,idx|
    a = paths[0].traverse_to(inter)    
    b = paths[1].traverse_to(inter)
    dist = a+b
    puts "A: #{a}"    
    puts "B: #{b}"    
    puts "Dist: #{dist}"
    puts "Current low: #{low}"
    if low.nil? || dist < low
        low = dist
    end
end

puts "Best distance: #{low}"