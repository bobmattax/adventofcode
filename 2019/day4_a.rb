
input = IO.readlines("day4.input")[0]
parts = input.split("-")
bottom = parts[0]
top = parts[1]

# bottom = 123456
# top = 123459

def decreases(x)    
    chars = x.to_s.chars
    for i in 0..chars.size-2 do
        if chars[i] > chars[i+1]
            return true
        end       
    end    
    return false
end

def no_adjacent_digits(x)
    chars = x.to_s.chars
    for i in 0..chars.size-2 do
        if chars[i] == chars[i+1]
            return false
        end       
    end 
    return true
end

def triple_digits(x)
    chars = x.to_s.chars
    for i in 0..chars.size-3 do
        if chars[i] == chars[i+1] && chars[i] == chars[i+2]    
            # puts "we found trip+ chars -- #{x}"
            return true
        end
    end 
    return false
end

my_array = [111111,112233,123444,223450,123789]
my_array = (355555..355777)
good_nums = []
trips = []
trip_w_dup = []
my_array = (bottom..top).to_a

my_array.to_a.each do |x|        
    if decreases(x)
        # puts "#{x} is no good: it decreases."        
        next
    end
    if no_adjacent_digits(x)
        # puts "#{x} is no good: it has no adjacent same digits."
        next
    end
    if triple_digits(x)
        trips << x
        puts "starting: #{x}----------------"
        matches = x.to_s.match /((\d)\2{2,6})+/
        # puts "Matches: #{matches}"
        remainder = x.to_s.split(matches[0])
        puts "Remainder: #{remainder}"
        dup = nil
        remainder.each do |part|
            if part.match /(\d)\1/
                puts "there's still a dup left"
                trip_w_dup << x
                dup = true
            else
                # puts "bad"
                dup = false
            end
        end
        if !dup
            puts "#{x} is no good: it has too many adjacent same digits and no other dups."
            next
        else
            # nothing
        end
    end
    good_nums << x
    # puts "#{x} is good! It passed all the tests"
end

puts good_nums.count
puts trips.inspect
puts trips.count
puts trip_w_dup.inspect
puts trip_w_dup.count