
a = IO.readlines("day5.input")
arr = a[0].split(",")
arr = arr.map(&:to_i)

input = "1"
outputs = []

# arr[1] = 12
# arr[2] = 2

puts "Initial:"
puts arr.inspect

def status(arr)
    puts arr
end

def parse_op(op_chunk)

    op = op_chunk.to_s.length < 3 ? op_chunk.to_i : op_chunk.to_s.slice(-2,2).to_i 

    puts "Op: #{op}"
    res = []
    res << op.to_i
    op_chunk.to_s.chars.reverse.each_with_index do |c,idx|
        next if idx == 1 || idx == 0
        res << c.to_i
    end
    puts "RESULT: #{res}"
    return res
end
class Array
    def get_val(mode,parm)
        mode = mode.nil? ? 0 : mode
        puts "Getting value: mode-#{mode} value-#{parm} "
        if mode == 1
            # immediate
            puts "-- #{self[parm]}"
            return self[parm].to_i
        else
            # position -- default
            puts "-- #{self[self[parm]]}"
            return self[self[parm]].to_i
        end
    end
end

count = 0
op_pos = 0
pause = nil
loop do    
    puts "============================"
    op = arr[op_pos]
    puts "#{op}"
    op_parts = parse_op(op)
    puts "Op parts: #{op_parts.inspect}"
    count += 1
    case op_parts[0].to_i
    when 1
        in_1 = arr.get_val(op_parts[1],op_pos+1).to_i
        in_2 = arr.get_val(op_parts[2],op_pos+2).to_i
        output = arr[op_pos+3]
        arr[output] = in_1 + in_2 
        puts "Adding #{in_1} and #{in_2} and storing in #{output}"
        pos_delta = 4

    when 2        
        in_1 = arr.get_val(op_parts[1],op_pos+1)
        in_2 = arr.get_val(op_parts[2],op_pos+2)
        output = arr[op_pos+3]
        arr[output] = in_1 * in_2 
        pos_delta = 4
    when 3
        puts "Current op pos: #{arr[op_pos]} #{arr[op_pos+1]}"
        puts "Requesting input:"
        in_1 = arr[op_pos+1]
        input = $stdin.gets.chomp
        puts "Received: #{input} "
        arr[in_1] = input
        puts "Storing input in #{in_1} -- proof -- #{arr[in_1]}"
        pos_delta = 2
    when 4
        puts "Outputting a thing:"
        in_1 = arr.get_val(op_parts[1],op_pos+1)
        puts "here: #{arr[in_1]}"
        puts "Pausing to read..."
        pause = $stdin.gets.chomp
        outputs << arr[in_1]
        pos_delta = 2
    when 5
        puts "Jump if True:"
        in_1 = arr.get_val(op_parts[1],op_pos+1)
        in_2 = arr.get_val(op_parts[2],op_pos+2)
        if in_1 != 0
            puts "Non-zero == JUMPING TO #{in_2}"
            pos_delta = 0
            op_pos = in_2
        else
            pos_delta = 2
        end
    when 6
        puts "Jump if False:"
        in_1 = arr.get_val(op_parts[1],op_pos+1)
        in_2 = arr.get_val(op_parts[2],op_pos+2)
        if in_1 == 0
            puts "Zero == JUMPING TO #{in_2}"
            pos_delta = 0
            op_pos = in_2
        else
            pos_delta = 2
        end
    when 7
        puts "less than:"
        in_1 = arr.get_val(op_parts[1],op_pos+1)
        in_2 = arr.get_val(op_parts[2],op_pos+2)
        in_3 = arr.get_val(op_parts[3],op_pos+3)
        if in_1 < in_2
            arr[in_3]
            puts "Storing [1] IN #{in_2} -- #{arr[in_2]}"
            pos_delta = 0
            op_pos = in_2
        else
            pos_delta = 2
        end
    when 8
    when 99
        puts "halt"
        puts "break 1"
        break
    end
    
    op_pos = op_pos + pos_delta
    if op_pos > arr.size || op == 99 #|| count > 10
    
        puts "break 2"
        break
    end
    puts "#{count} : #{op}"
    puts "Status: #{arr.inspect}"
    puts "op pos: #{op_pos}"
    puts "--------------------------"
end
puts arr.inspect
puts outputs.inspect

puts "FINAL: #{arr.inspect}"
