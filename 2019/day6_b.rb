input= IO.readlines("day6.input").collect{|x|x.chomp}
require 'tree'

# puts input.inspect
@nodes = []

def status
    puts "Plain nodes: #{@nodes.map{|x| x.name } }"
    puts "Tree nodes: "
    @nodes.map{|x| x.print_tree}
end

def find_common_ancestor(tree,start,finish)
    #find start in tree
    start = tree.find{|x| x.name == start}
    finish = tree.find{|x| x.name == finish}

    puts start.parentage
    puts "^^^^^^^^^^^^^^^^"
    puts finish.parentage
    
    overlap = start.parentage.collect{|x| x.name} & finish.parentage.collect{|x| x.name}
    # puts "overlap #{overlap}"
    return tree.find{|x| x.name == overlap[0] }

end

def distance_between_nodes(tree,start,finish)
    puts "Common node = #{finish}"
    count = 0
    start = tree.find{|x| x.name == start}
    finish = tree.find{|x| x.name == finish}
    start.parentage.each do |n| 
        count += 1
        puts "#{n.name}"
        if n == finish
            return count
        end
        
    end
    return count
end

class Array
    def find_in_all(name)
        # puts "Looking for #{name} in nodes..."
        # puts "look in plain nodes first..."
        plain = self.select{|x| x.name == name}
        # puts "in plain? #{plain[0]}"
        # puts "Plain nodes: #{self.map{|x| x.name } }"
        if !plain.empty?
            return plain[0]
        end
        # puts "wasn't found in flat array... looking in children for [#{name}]- *************** "
        nodes = self.collect do |x|
            # puts "looking for [#{name}] in each 'root' -- #{x.name}"
            found = x.find{|i| i.name == name}

            # puts "child here? {#{found.inspect}}"
            x.find{|i| i.name == name}
        end
        # puts "Nodes after tree search: #{nodes.reject{|x| x.nil?}[0]}"
        if nodes.empty?
            return nil
        else
            # puts "returning node[0]"
            return nodes.reject{|x| x.nil?}[0]
        end
    end
end

def find_or_create_node(name)
    # puts "Creating/finding [#{name}]"
    if @nodes.find{|x| x.name == name}
        #it exists
    else
        node = Tree::TreeNode.new(name)
        @nodes << node
    end
    return @nodes.find{|x| x.name == name}
end

def parse(instruction)
    parts = instruction.split(")")
    find_or_create_node(parts[0])
    find_or_create_node(parts[1])
end

def assemble(instruction)
    # puts "-----------------------------------"
    # status
    parts = instruction.split(")")
    # puts "Assembling #{parts[0]} << #{parts[1]}"
    node_1 = @nodes.find_in_all(parts[0])
    # puts "NODE 1: #{node_1}"
    node_2 = @nodes.find_in_all(parts[1])
    # puts "NODE 2: #{node_2}"
    node_1 << node_2
    # remove the node 2 from @nodes
    # puts "removing node 2 [#{node_2.name}] from @nodes"
    @nodes = @nodes.reject{|x| x.name == parts[1]}
end

input.each do |instr|
    parse(instr)
end

go = nil
input.each do |instr|
    # puts "==================================="
    assemble(instr)
    # go = $stdin.gets.chomp
end

# puts @nodes.map{|x| x.name }

puts @nodes[0]

puts "NOde count: #{@nodes.count}"
puts @nodes[0].print_tree

@root = @nodes[0]

sum = 0
@root.each do |node|
    # puts "LEAF? - #{node.is_leaf?}"
    if !node.is_root?
        # puts "Parentage - #{node.parentage.count}"
        sum += node.parentage.count
    end
end

puts "total orbits: #{sum}"
common = find_common_ancestor(@root,"YOU","SAN")
puts "Common Ancestor: #{common}"
dist_1 = distance_between_nodes(@root,"YOU",common.name)
dist_2 = distance_between_nodes(@root,"SAN",common.name)
puts "Distance from YOU to ancester: #{dist_1}"
puts "Distance from SAN to ancester: #{dist_2}"
puts "Total: #{dist_1 + dist_2 - 2}"



# create nodes
# assemble nodes





