input_file = 'day_1.sample.txt'
input_file = 'day_1.input.txt'

increased = 0
decreased = 0
current = nil
previous = nil
File.open(input_file).each do |line|
    current = line
    puts "Comparing #{previous} and #{current}"
    if previous.nil? || current > previous 
        increased += 1
    else
        # nothing
    end
    puts "RESULT: #{increased}"
    previous = current
    # blah gets   
end

puts "INCREASED COUNT: #{increased}"

