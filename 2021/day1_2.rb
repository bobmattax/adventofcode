input_file = 'day_1.sample.txt'
input_file = 'day_1.input.txt'

increased = 0
decreased = 0
current = nil
previous = nil
cursor = 0
window_count = 0
input = File.readlines(input_file).map(&:chomp).map(&:to_i)
window_size = 2
puts input.size

while(cursor < input.size - window_size)
    puts "============================="
    puts "Window: #{cursor} : #{cursor+1} : #{cursor + 2}"
    puts "Window: #{input[cursor]} : #{input[cursor+1]} : #{input[cursor+2]}"
    current = (input[cursor] + input[cursor+1] + input[cursor+2])
    puts "CURRENT: #{current}"
    if !previous.nil? && current > previous 
        increased += 1
        puts "INCREASED +++++++"
    else
        # nothing
    end
    previous = current
    cursor +=1
    window_count +=1 
end
puts "Total windows: #{window_count}"

#     current = line
#     puts "Comparing #{previous} and #{current}"
#     if previous.nil? || current > previous 
#         increased += 1
#     else
#         # nothing
#     end
#     puts "RESULT: #{increased}"
#     previous = current
#     # blah gets   

puts "INCREASED COUNT: #{increased}"

