input_file = 'day_2.sample.txt'
input_file = 'day_2.input.txt'

depth = 0
position = 0

File.open(input_file).each do |line|
    op = line.split(" ")
    command = op[0]
    distance = op[1].to_i
    pp op
    case command
        when "forward"
            position = position + distance
        when "down"
            depth = depth + distance
        when "up"
            depth = depth - distance
    end
    
end
puts "FINAL POSTION: #{depth} : #{position} "
puts "result: #{depth * position}"

# puts "INCREASED COUNT: #{increased}"

