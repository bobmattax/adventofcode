input_file = 'day_3.sample.txt'
input_file = 'day_3.input.txt'

gamma = nil
epsilon = nil



input = File.readlines(input_file).map(&:chomp)

graph = []

input.each do |row|
    graph << row.split("").map(&:to_i)
end

trans = graph.transpose

gamma = ""
epsilon = ""

trans.each do |row|
    zeros = row.count(0)
    ones = row.count(1)
    if zeros > ones
        gamma += "0"
        epsilon += "1"
    else
        gamma += "1"
        epsilon += "0"
    end
end

pp gamma
pp epsilon

num_gamma = gamma.to_i(2)
num_epsilon = epsilon.to_i(2)

power_consumption = num_epsilon * num_gamma
puts power_consumption