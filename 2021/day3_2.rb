input_file = 'day_3.sample.txt'
input_file = 'day_3.input.txt'

gamma = nil
epsilon = nil

def oxy_gen_rating(graph)
    trans = graph.transpose
    
    bit = 0
    while ( bit < graph[0].size && graph.size > 1 )
        puts "================================="
        puts "Bit #{bit}"
        puts "Trans[#{bit}] #{trans[bit]}"
        most = trans[bit].count(1) >= trans[bit].count(0) ? 1 : 0
        puts "Most common : #{most}"
        graph = graph.select{ |x| x[bit] == most}
        puts "New graph" 
        # pp graph
        trans = graph.transpose
        puts "BIT @ end: #{bit} | Graph Size: #{graph.size}"
        
        bit +=1
    end
    puts "RESULTING GRAPH: #{graph[0].join().to_i(2)}"
    return graph[0].join().to_i(2)
end

def co2_rating(graph)
    trans = graph.transpose
    
    bit = 0
    while ( bit < graph[0].size && graph.size > 1 )
        puts "================================="
        puts "Bit #{bit}"
        puts "Trans[#{bit}] #{trans[bit]}"
        least = trans[bit].count(0) <= trans[bit].count(1) ? 0 : 1
        puts "Least common : #{least}"
        graph = graph.select{ |x| x[bit] == least}
        puts "New graph" 
        # pp graph
        trans = graph.transpose
        puts "BIT @ end: #{bit} | Graph Size: #{graph.size}"
        
        bit +=1
    end
    puts "RESULTING GRAPH: #{graph[0].join().to_i(2)}"
    return graph[0].join().to_i(2)
end


input = File.readlines(input_file).map(&:chomp)

graph = []

input.each do |row|
    graph << row.split("").map(&:to_i)
end

oxy = oxy_gen_rating(graph)
puts "OXYGEN: #{oxy}"
co2 = co2_rating(graph)
puts "CO2: #{co2}"

puts "life support: #{co2 * oxy }"

# trans = graph.transpose

# gamma = ""
# epsilon = ""

# bit = 0
# while ( bit < graph[0].size && graph.size > 1 )
#     puts "================================="
#     puts "Bit #{bit}"
#     puts "Trans[#{bit}] #{trans[bit]}"
#     most = trans[bit].count(1) >= trans[bit].count(0) ? 1 : 0
#     puts "Most common : #{most}"
#     graph = graph.select{ |x| x[bit] == most}
#     puts "New graph" 
#     pp graph
#     trans = graph.transpose
#     puts "BIT @ end: #{bit} | Graph Size: #{graph.size}"
    
#     bit +=1
# end
