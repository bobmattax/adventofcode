input_file = 'day_4.sample.txt'
input_file = 'day_4.input.txt'
class Array
    def split(value = nil)
        arr = dup
        result = []
        if block_given?
          while (idx = arr.index { |i| yield i })
            result << arr.shift(idx)
            arr.shift
          end
        else
          while (idx = arr.index(value))
            result << arr.shift(idx)
            arr.shift
          end
        end
        result << arr
    end
end

def grid_smash(g1, g2, condition, operation)
    sum = 0
    g2.each_with_index do |row,i|
        g2[i].each_with_index do |col,j|
            if(g2[i][j].to_i == condition.to_i)
                sum += g1[i][j].to_i
            end
        end
    end

    return sum
end


def create_board(board_input)
    grid = []
    # puts "creating board"
    # pp board_input
    grid = board_input.map{|x| x.split(" ").map(&:to_i)}
end

def number_in_board(number,board)
    row = board.detect{|aa| aa.include?(number)}
    if row
        return [row.index(number), board.index(row)]
    else
        return nil
    end
end

def check_bingo(numbers, board, verbose = false, return_grid = false)
    win_grid = (board.size).times.map{(board.size).times.map { 0 }}
    # puts "Checking board"
    # puts " --- #{numbers}"
    win = false
    numbers.each do |num|
        res = number_in_board(num,board)
        win_grid[res[1]][res[0]] = 1 if res
    end
    pp win_grid if verbose
    win_grid_90 = win_grid.transpose
    if return_grid
        return win_grid
    else
        return winning_row(win_grid) || winning_row(win_grid_90)
    end
end

def winning_row(grid)
    win = false
    grid.each do |row|
        win = true if row.count(1) == 5
    end
    return win
end

input = File.readlines(input_file).map(&:chomp)

bingo_numbers = input.shift().split(",").map(&:to_i)
input.shift() # get rid of extra blank line
board_input = input.split("")

boards = board_input.map{|x| create_board(x)}

# loop through numbers, checking win on each board 
winning_board = nil
iteration_max = 0
board_wins = (boards.size).times.map{0}
bingo_numbers.each_with_index do |n, i|
    # next if winning_board
    # puts "============================================"
    # puts "Drawing number #[#{i}] -- #{n}"
    # puts "-----------"
    # puts "Checking boards"
    win = nil
    
    

    boards.each_with_index do |b,j|
        # next if winning_board
        win = check_bingo(bingo_numbers.slice(0,i+1),b)
        iteration_max = i if win
        winning_board = j if win
        board_wins[j] = 1 if win
    end
    pp board_wins
    if board_wins.count(0) == 1
        break
        # blah = gets.chomp 
        next
    end
    
    # break if winning_board
    # break if i > 12
end
puts "BOARD WINS"
pp board_wins
loser = board_wins.find_index(0)

losing_board = boards[loser]

puts "LOSER: #{loser}"
puts "FOUND AT #{iteration_max} iterations"
# print winning board
pp losing_board

while !check_bingo(bingo_numbers.slice(0,iteration_max),losing_board)
    puts "Trying w/ #{iteration_max} ---------------------"
    pp bingo_numbers.slice(0,iteration_max)
    pp check_bingo(bingo_numbers.slice(0,iteration_max),losing_board,false,true)
    iteration_max +=1
end
pp bingo_numbers.slice(0,iteration_max)
final_num = bingo_numbers[iteration_max-1]
puts "Finally found it at #{iteration_max} : #{bingo_numbers[iteration_max-1]}"

# print winning grid
win_grid = check_bingo(bingo_numbers.slice(0,iteration_max),losing_board,false,true)
pp win_grid

num = grid_smash(losing_board, win_grid, 0, "sum")
pulled = bingo_numbers[iteration_max-1]

puts "#{num} * #{pulled} = #{num * pulled}"
# puts check_bingo(bingo_numbers.slice(0,12),boards[2])





