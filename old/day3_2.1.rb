blah = "
37	36	35	34	33	32	31
38	17	16	15	14	13	30
39	18	5 	4 	3 	12	29
40	19	6 	1 	2 	11	28
41	20	7 	8 	9 	10	27
42	21	22	23	24	25	26
43	44	45	46	47	48	49
"

# east = 1,2,11,28,53 (25 + 3)      (49 + 8 * 1 - 4)
# nort = 1,4,15,34,61 (25 + 3 + 6)  (49 + 8 * 2 - 4)
# west = 1,6,19,40,69 (25 + 3 + 12) (49 + 4 + 16)
# sout = 1,8,23,46,77 (25 + 3 + 18) (49 + 4 + 24)

def middles(sz)
	return (1..4).collect{|x| (sz-2)**2 + ((sz-1) * x) - ((sz-1) / 2)}
end

def nearest_center()
end

input = 289326
square = Math.sqrt(input)
ceil = square.ceil
puts ceil

if ceil % 2 == 0
	box = ceil + 1
else
	box = ceil
end


puts "Box: #{box}"
puts "Middles: #{middles(box)}"
center = (box+1) / 2 -1
middle = middles(box).collect{|x| (x - input).abs}.min
puts "Distance to center: #{(box+1) / 2 -1}"
puts "Distance to middle: #{middles(box).collect{|x| (x - input).abs}.min}"
puts "Steps: #{center + middle}"



# find the nearest, odd square above the number - that's the bounding box
# the nearest even square below it +1 is the lowest value of the box
# odd ^ 2 - (odd / 2)
# 7** - (7 - 1) / 2
# east =
