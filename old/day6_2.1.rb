input = "0	5	10	0	11	14	13	4	11	8	8	7	1	4	12	11"
input = "0\t2\t7\t0"
banks = input.split("\t").collect{|x| x.to_i}

steps = 0
perms = {}
puts "Starting input #{banks.inspect}"
loop do
  max = banks.max
  idx = banks.index(max)
  puts "Max: #{max}"
  redis = max
  puts "redistributing #{max} things starting at #{idx}"
  puts "set #{idx} to zero"
  banks[idx] = 0
  loop do
    idx = (idx + 1) % banks.size
    banks[idx] += 1
    puts "redis: index: #{idx} redis: #{redis} bank[#{idx}]#{banks[idx]}:"
    redis -= 1
    break if redis == 0
  end
  perm = banks.inspect.to_s
  steps += 1
  if perms.keys.include?(perm)
    puts "Total steps: #{steps}"
    puts "Loop size: #{steps - perms[perm]}"
    break
  else
    perms[perm]=steps
  end
end
